const path = require('path');

module.exports = {
  entry: __dirname + '/src/index.js',

  output: {
    path: __dirname + '/dist',
    filename: 'bundle.js'
  },

  resolve: {
    alias: {
      "components": path.resolve(__dirname, './src/components/'),
      "images": path.resolve(__dirname, './src/images')
    }
  },

  module: {
    rules : [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              "@babel/preset-react",
              ["@babel/preset-env", {
                useBuiltIns: "usage",
                corejs: 3,
                targets: { browsers: "defaults" }
              }]
            ],
            plugins: [
              "babel-plugin-styled-components",
              "@babel/plugin-proposal-class-properties"
            ]
          }
        }
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        exclude: /node_modules/,
        use: [{
          loader: 'file-loader',
          options: { outputPath: 'images/' }
        }]
      }
    ]
  },

  plugins: [],

  devServer: {
    contentBase: path.join(__dirname, "/dist")
  }
};
