import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import Icon from '@mdi/react';
import { mdiClockOutline } from '@mdi/js';

import imageDownload from 'images/download.png';

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  background-color: #c0c0c0;
  background-image: url(${imageDownload});
`;

const BodyText = styled.span`
  font-size: 25px;
`;

const asyncTimeout = (ms = 1000) =>
  new Promise((resolve) => {
    setTimeout(resolve, ms);
  });

const App = ({ bodyText }) => {
  const [testString, setTestString] = useState('Initial test string');

  useEffect(() => {
    (async () => {
      await asyncTimeout();
      setTestString('Final test string');
    })();
  }, [setTestString]);

  return (
    <Wrapper>
      <BodyText>{`${bodyText}:${testString}`}</BodyText>
      <img src={imageDownload} alt="Download" />
      <Icon path={mdiClockOutline} size={1} color="white" />
    </Wrapper>
  );
};

App.propTypes = {
  bodyText: PropTypes.string,
};

App.defaultProps = {
  bodyText: 'No title!',
};

export default App;
