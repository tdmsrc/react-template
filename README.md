# React Template

- Basic setup for `Babel` + `Webpack` + `ESLint` + `Prettier` + `React`
- Styles all come from `styled-components`
- Linting uses `eslint-config-airbnb` with some modifications
- Use `npm run start` or `npm run build`
